## [0.1.6](https://gitlab.com/soccrates-eu/whoami/compare/0.1.5...0.1.6) (2022-06-25)


### Bug Fixes

* drops ingress ([2d77dd4](https://gitlab.com/soccrates-eu/whoami/commit/2d77dd42e40ba39e70946cddab213a1167154363))

## [0.1.5](https://gitlab.com/soccrates-eu/whoami/compare/0.1.4...0.1.5) (2022-06-24)


### Bug Fixes

* testvalues.yaml ([0ecbd02](https://gitlab.com/soccrates-eu/whoami/commit/0ecbd02f58805a403f1e92936714444b5c199331))

## [0.1.4](https://gitlab.com/soccrates-eu/whoami/compare/0.1.3...0.1.4) (2022-06-24)


### Bug Fixes

* templates namespace ([7ac684c](https://gitlab.com/soccrates-eu/whoami/commit/7ac684cc8ddcd2334e89aced6762af382bafc6ef))

## [0.1.3](https://gitlab.com/soccrates-eu/whoami/compare/0.1.2...0.1.3) (2022-06-24)


### Bug Fixes

* makes domain global variable ([33c3ed1](https://gitlab.com/soccrates-eu/whoami/commit/33c3ed1d25fdeea7c47db6754f7ef137b943a74c))

## [0.1.2](https://gitlab.com/soccrates-eu/whoami/compare/0.1.1...0.1.2) (2022-06-24)


### Bug Fixes

* update readme ([1962d53](https://gitlab.com/soccrates-eu/whoami/commit/1962d53bd55e995ffaf49dca5bf534fc83b29ce6))

## [0.1.1](https://gitlab.com/soccrates-eu/whoami/compare/0.1.0...0.1.1) (2022-06-24)


### Bug Fixes

* adds repositoryID from artifacthub ([ca50762](https://gitlab.com/soccrates-eu/whoami/commit/ca5076268ad4e5a25ec3a11e5eb7efecc4998dd0))
